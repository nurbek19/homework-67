import React, {Component} from 'react';
import {connect} from 'react-redux';
import './DoorKeyboard.css';
class DoorKeyboard extends Component {

    render() {
        let stars = '';
        for(let i = 0; i < this.props.enteredPassword.length; i++) {
            stars += '*';
        }

        const accessGranted = 'Access Granted';

        return (
            <div className="keyboard">
                <div className={this.props.access ? 'screen green' : 'screen'}>
                    {this.props.access ? accessGranted : stars}
                </div>
                {this.props.numbers.map(number => {
                    if(number === 'E') {
                        return <button onClick={this.props.checkPassword} key={number}>{number}</button>;
                    } else if(number === '<') {
                        return <button onClick={this.props.clearNumber} key={number}>{number}</button>;
                    } else {
                        return <button onClick={this.props.clickButton} key={number}>{number}</button>;
                    }
                })}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        enteredPassword: state.enteredPassword,
        numbers: state.numbers,
        access: state.access
    };
};

const mapDispatchToProps = dispatch => {
    return {
        clickButton: (event) => dispatch({type: 'ADD', value: event.target.innerHTML}),
        checkPassword: () => dispatch({type: 'CHECK'}),
        clearNumber: () => dispatch({type: 'CLEAR'})
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(DoorKeyboard);