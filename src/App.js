import React, { Component } from 'react';
import DoorKeyboard from './containers/DoorKeyboard/DoorKeyboard';
import './App.css';

class App extends Component {
  render() {
    return (
      <DoorKeyboard/>
    );
  }
}

export default App;
