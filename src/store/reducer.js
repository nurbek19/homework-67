const initialState = {
    enteredPassword: '',
    password: '9071',
    numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, '<', 0, 'E'],
    access: false
};

const reducer = (state = initialState, action) => {
    if (state.enteredPassword.length < 4) {
        if (action.type === 'ADD') {
            return {
                ...state,
                enteredPassword: state.enteredPassword + action.value
            }
        }
    }

    if (action.type === 'CHECK') {
        if (state.enteredPassword === state.password) {
            return {
                ...state,
                access: true
            }
        } else {
            alert('Incorrect password');
            return {
                ...state,
                enteredPassword: ''
            }
        }
    }

    if (state.enteredPassword.length > 0) {
        if (action.type === 'CLEAR') {
            return {
                ...state,
                enteredPassword: state.enteredPassword.substring(0, state.enteredPassword.length - 1)
            }
        }
    }

    return state;
};

export default reducer;
